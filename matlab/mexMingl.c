/// Implements imshow like functionality.
/// Usage: 
///     glshow(im,[low high]);
///
///     im   -  a 2d grayscale image
///     low  -  minimum intensity to use for contrast scaling
///     high -  maximum intensity to use for contrast scaling
#include <mex.h>
#include "app.h"
#include "imshow.h"
#include <stdarg.h>
#include <stdio.h>

#define LOG(...) logger(0,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define ERR(...) logger(1,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define CHECK(e) do {if(!((e))) {ERR("Expression was false.\n%s\n",#e); goto Error;}} while(0)

static void logger(int is_error,const char *file,int line,const char* function,const char *fmt,...) {
    char buf1[1024]={0},buf2[1024]={0};
    va_list ap;
    va_start(ap,fmt);
    vsprintf(buf1,fmt,ap);
    va_end(ap);                               
    #if 0
        sprintf(buf2,"%s(%d): %s()\n\t - %s\n",file,line,function,buf1);
    #else
        sprintf(buf2,"%s\n",buf1);
    #endif
    if(is_error)
        mexErrMsgTxt(buf2);
    else
        mexPrintf(buf2);
}

static enum imshow_scalar_type fromClassID(mxClassID cid,int *is_ok) {
    if(is_ok) *is_ok=1;
    switch(cid) {
        // case mxLOGICAL_CLASS:  return imshow_u32;  // ? not sure how this is stored
        // case mxCHAR_CLASS:     return imshow_u8;   // ? probably reqires string translation
        case mxDOUBLE_CLASS:   return imshow_f64;
        case mxSINGLE_CLASS:   return imshow_f32;
        case mxINT8_CLASS:     return imshow_i8;
        case mxUINT8_CLASS:    return imshow_u8;
        case mxINT16_CLASS:    return imshow_i16;
        case mxUINT16_CLASS:   return imshow_u16;
        case mxINT32_CLASS:    return imshow_i32;
        case mxUINT32_CLASS:   return imshow_u32;
        case mxINT64_CLASS:    return imshow_i64;
        case mxUINT64_CLASS:   return imshow_u64;
        default:;
    }
    if(is_ok) *is_ok=0;
    return imshow_u8;
}

static int validate_input(int nrhs, const mxArray **prhs) {
    CHECK(nrhs==1);
    CHECK(mxGetNumberOfDimensions(prhs[0])==2);
    int is_pixel_type_supported;
    fromClassID(mxGetClassID(prhs[0]),&is_pixel_type_supported);
    CHECK(is_pixel_type_supported);
    return 1;
Error:
    return 0;
}

/// Implements imshow like functionality.
/// Usage: 
///     glshow(im,[low high]);
///
///     im   -  a 2d grayscale image
///     low  -  minimum intensity to use for contrast scaling
///     high -  maximum intensity to use for contrast scaling
///
/// First call will open a window.  The texture displayed in the 
/// window will be updated on subsequent calls.

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    CHECK(validate_input(nrhs,prhs));
    mexAtExit(app_teardown); // teardown signals close and blocks till everything is done closing down
    app_init(logger);        // only inits on the first call

    size_t 
        w=mxGetM(prhs[0]),
        h=mxGetN(prhs[0]);

    imshow_contrast(imshow_u8,0,255);
    imshow(fromClassID(mxGetClassID(prhs[0]),0),w,h,mxGetData(prhs[0]));

Error:;
}

